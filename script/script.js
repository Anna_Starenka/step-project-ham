// let titleService = document.querySelector('.tabs-services')
// titleService.addEventListener('click',(ev)=>{
    
//    let data= ev.target.dataset.tab;
   
//    document.querySelector('.active-tab').classList.remove('active-tab')
//    document.querySelector('.active-text').classList.remove('active-text')

//    document.querySelector(`[data-text = ${data}]`).classList.add('active-text')

//    ev.target.classList.add('active-tab')
// })

// let titlWork = document.querySelector('.tab-works')


let titleService = document.querySelector('.tabs-services')
titleService.addEventListener('click', tab)
    


function tab(ev) {
   let data= ev.target.dataset.tab;
   
   document.querySelector('.active-tab').classList.remove('active-tab')
   document.querySelector('.active-text').classList.remove('active-text')

   document.querySelector(`[data-text = ${data}]`).classList.add('active-text')

   ev.target.classList.add('active-tab')
}


let titleWork = document.querySelector('.tab-works')
titleWork.addEventListener('clck', tab)


const arrOfImages =[
   'https://cityhost.ua/upload_img/blog5fdc5278825dd_90b5c0862736780ddeab95d252611c3f.png',
   'https://www.tophosting.in.ua/wp-content/uploads/2016/07/9-%D0%BB%D1%83%D1%87%D1%88%D0%B8%D1%85-%D0%BF%D0%BB%D0%B0%D0%B3%D0%B8%D0%BD%D0%BE%D0%B2-%D1%80%D0%B5%D0%B7%D0%B5%D1%80%D0%B2%D0%BD%D0%BE%D0%B3%D0%BE-%D0%BA%D0%BE%D0%BF%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F-%D0%BD%D0%B0-WordPress-1.jpg',
   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSc20teGnS6Ra1K47ycA4ck9cazpethAhwMkQ&usqp=CAU',
   'https://moosend.com/wp-content/uploads/2019/03/LILT-social-proof-on-landing-page-1024x1003.png',
   'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSoAsYtVhwAWW506zlFHoi1nd-73jvABI1gsg&usqp=CAU',
   'https://business.adobe.com/blog/basics/media_1989cc7d902aa64ab900604ff44f877f8eae74ac4.png?width=750&format=png&optimize=medium',
   'https://quasa.io/storage/photos/%D1%84%D0%BE%D1%82%D0%BE%2016/1%2010.jpeg',
   'https://www.newperspectivestudio.co.za/wp-content/uploads/2023/02/tips-for-hiring-a-web-designer-in-South-africa.jpg',
   'https://www.acelema.com/blog/wp-content/uploads/2022/07/1_5IYtIe5OwxeoYXi226J-Uw.jpeg',
   'https://www.netleafinfosoft.com/our-blog/wp-content/uploads/2019/10/Graphic-Design-Services-in-Gurgaon.jpg',
   'https://sunshinedesign.com.au/wp-content/uploads/2023/02/stufly_graphic_design_colourful_birds_exploding_out_powder_99bdc5ea-e29b-4c0a-986e-e9f557fdc032-1024x675.png',
   'https://www.devstars.com/wp-content/uploads/2023/02/stuart_visual_representation_of_the_future_of_graphic_design_wi_35d8b2a5-a224-4063-8239-359e0ffe4ccd-1-1024x512.jpg'
]

const btnLoad = document.querySelector('.load')



const imgContainer = document.querySelector('.body-tabs-works')



btnLoad.addEventListener('click', (ev)=>{
   const fragment = document.createDocumentFragment()
   arrOfImages.forEach(src => {
      const img = createImageTemplate(src)
      fragment.append(img)
   });

   imgContainer.append(fragment)
   
   const imgHover = document.querySelector('.image-hover')
   imgContainer.prepend(imgHover)
   btnLoad.style.display = 'none'
})

function createImageTemplate(src) {
   const img = document.createElement('img')
   img.setAttribute('src', src)
   img.setAttribute('alt','data-img');
   img.setAttribute('width','285')
   img.setAttribute('height', '211')
   return img
}